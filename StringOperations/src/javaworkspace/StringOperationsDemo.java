package javaworkspace;
import javaworkspace.StringOperation;

public class StringOperationsDemo {
	public static void main(String args []) {
		StringOperation stropp = new StringOperation();
		String name1 = "java is good Programming language";
		String name2 = "corona virus";
		String name3= "good";
		String name4= "Programming";
		String name5= "HELLO WORLD";
		String name6= "                          HELLO WORLD";

		
		stropp.CharAt(name1);
		stropp.concat(name1,name4);
		stropp.indexof(name1);
		stropp.indexof1(name2);
		stropp.indexof2(name1,name3);
		stropp.indexof3(name1,name4);
		stropp.lastindexof(name2);
		stropp.lastindexof1(name1);
		stropp.lastindexof2(name1,name4);
		stropp.lastindexof3(name1);
		stropp.length(name1);
		stropp.replace(name1);
		stropp.startsWith(name1);
		stropp.substring(name1);
		stropp.substring1(name1);
		stropp.tolowercase(name5);
		stropp.toupercase(name2);
		stropp.trim(name6);


				
	}
	
}
/*
output

1.Returns the character at the specified index 
 string index Value :P

----------------------------------
5.Concatenates the specified string to the end of this string 
 string index Value :java is good Programming languageProgramming
----------------------------------

16.Returns the index within this string of the first occurrence of the
specified character.
 string index Value :1
----------------------------------

17.Returns the index within this string of the first occurrence of the
specified character, starting the search at the specified index.
 string index Value :5
----------------------------------

18.Returns the index within this string of the first occurrence of the
specified substring.
 string index Value :8
----------------------------------

19.Returns the index within this string of the first occurrence of the
specified substring, starting at the specified index.
 string index Value :13
----------------------------------

21.Returns the index within this string of the last occurrence of the specified
character.
 string index Value :5
----------------------------------

22.Returns the index within this string of the last occurrence of the specified
character, searching backward starting at the specified index.
 string index Value :3
----------------------------------

23.Returns the index within this string of the rightmost occurrence of the
specified substring.
 string index Value :13
----------------------------------

24.Returns the index within this string of the last occurrence of the specified
substring, searching backward starting at the specified index..
 string index Value :10
----------------------------------

25.Returns the length of this string.
 string index Value :33
----------------------------------

29.Returns a new string resulting from replacing all occurrences of oldChar
in this string with newChar.
 string index Value :java is gFFd PrFgramming language
----------------------------------

34.Tests if this string starts with the specified prefix.
 string startsWith true/false :true
 string startsWith true/false :false
----------------------------------

37.Returns a new string that is a substring of this string.
 substring Value :is good Programming language
----------------------------------

38.Returns a new string that is a substring of this string.
 substring Value : good Pr
----------------------------------

40.Converts all of the characters in this String to lower case using the rules
of the default locale.
 substring Value :hello world
----------------------------------

43.Converts all of the characters in this String to upper case using the rules
of the default locale.
 substring Value :CORONA VIRUS
----------------------------------

45.Returns a copy of the string, with leading and trailing whitespace
omitted.
 substring Value :HELLO WORLD
----------------------------------
/*