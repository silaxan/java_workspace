package javaworkspace;
public class StringOperation {
	public void CharAt(String name1){
		 char index = name1.charAt(13);
		  System.out.println("1.Returns the character at the specified index ");
	      System.out.println(" string index Value :" + index);
	      System.out.println();
	      System.out.println("----------------------------------");
		
	}
	
	public void concat(String name1,String name4){
		     String concat = name1.concat(name4);
			  System.out.println("5.Concatenates the specified string to the end of this string ");
		      System.out.println(" string index Value :" + concat);
		      System.out.println("----------------------------------");
		      System.out.println();
		
	}
	
	public void indexof(String name1){
		int index = name1.indexOf('a');
		  System.out.println("16.Returns the index within this string of the first occurrence of the\r\n" + 
		  		"specified character.");
	      System.out.println(" string index Value :" + index);
	      System.out.println("----------------------------------");
	      System.out.println();
	}
	

	public void indexof1(String name2){
		int index = name2.indexOf('a',2);
		  System.out.println("17.Returns the index within this string of the first occurrence of the\r\n" + 
		  		"specified character, starting the search at the specified index.");
	      System.out.println(" string index Value :" + index);
	      System.out.println("----------------------------------");
	      System.out.println();
	}
	
	public void indexof2(String name1, String name3){
		int index = name1.indexOf(name3);
		  System.out.println("18.Returns the index within this string of the first occurrence of the\r\n" + 
		  		"specified substring.");
	      System.out.println(" string index Value :" + index);
	      System.out.println("----------------------------------");
	      System.out.println();
	}	   
		
	public void indexof3(String name1, String name4){
		int index = name1.indexOf(name4,6);
		  System.out.println("19.Returns the index within this string of the first occurrence of the\r\n" + 
		  		"specified substring, starting at the specified index.");
	      System.out.println(" string index Value :" + index);
	      System.out.println("----------------------------------");
	      System.out.println();
	}	
	
	public void lastindexof(String name2){
		int index = name2.lastIndexOf('a');
		  System.out.println("21.Returns the index within this string of the last occurrence of the specified\r\n" + 
		  		"character.");
	      System.out.println(" string index Value :" + index);
	      System.out.println("----------------------------------");
	      System.out.println();
	}
		
	
	public void lastindexof1(String name1){
		int index = name1.lastIndexOf('a',7);
		  System.out.println("22.Returns the index within this string of the last occurrence of the specified\r\n" + 
		  		"character, searching backward starting at the specified index.");
	      System.out.println(" string index Value :" + index);
	      System.out.println("----------------------------------");
	      System.out.println();
	}
	
	public void lastindexof2(String name1,String name4){
		int index = name1.lastIndexOf(name4);
		  System.out.println("23.Returns the index within this string of the rightmost occurrence of the\r\n" + 
		  		"specified substring.");
	      System.out.println(" string index Value :" + index);
	      System.out.println("----------------------------------");
	      System.out.println();
	}
	
	public void lastindexof3(String name1){
		int index = name1.lastIndexOf('o',13);
		  System.out.println("24.Returns the index within this string of the last occurrence of the specified\r\n" + 
		  		"substring, searching backward starting at the specified index..");
	      System.out.println(" string index Value :" + index);
	      System.out.println("----------------------------------");
	      System.out.println();
	}
	
	public void length(String name1){
	int index = name1.length();
	  System.out.println("25.Returns the length of this string.");
    System.out.println(" string index Value :" + index);
    System.out.println("----------------------------------");
    System.out.println();
	}
	
	public void replace(String name1){
		String index = name1.replace('o','F');
		  System.out.println("29.Returns a new string resulting from replacing all occurrences of oldChar\r\n" + 
		  		"in this string with newChar.");
	      System.out.println(" string index Value :" + index);
	      System.out.println("----------------------------------");
	      System.out.println();
	}
	
	public void startsWith(String name1){
		boolean index = name1.startsWith("java");
		boolean index1 = name1.startsWith("good");
		  System.out.println("34.Tests if this string starts with the specified prefix.");
	      System.out.println(" string startsWith true/false :" + index);
	      System.out.println(" string startsWith true/false :" + index1);
	      System.out.println("----------------------------------");
	      System.out.println();
	}

	public void substring(String name1){
		String index = name1.substring(5);
		  System.out.println("37.Returns a new string that is a substring of this string.");
	      System.out.println(" substring Value :" + index);
	      System.out.println("----------------------------------");
	      System.out.println();
	}
	
	public void substring1(String name1){
		String index = name1.substring(7,15);
		  System.out.println("38.Returns a new string that is a substring of this string.");
	      System.out.println(" substring Value :" + index);
	      System.out.println("----------------------------------");
	      System.out.println();
	}
	
	public void tolowercase(String name5){
		String index = name5.toLowerCase();
		  System.out.println("40.Converts all of the characters in this String to lower case using the rules\r\n" + 
		  		"of the default locale.");
	      System.out.println(" substring Value :" + index);
	      System.out.println("----------------------------------");
	      System.out.println();
	}

	
	public void toupercase(String name2){
		String index = name2.toUpperCase();
		  System.out.println("43.Converts all of the characters in this String to upper case using the rules\r\n" + 
		  		"of the default locale.");
	      System.out.println(" substring Value :" + index);
	      System.out.println("----------------------------------");
	      System.out.println();
	}
	
	public void trim(String name6){
		String index = name6.trim();
		  System.out.println("45.Returns a copy of the string, with leading and trailing whitespace\r\n" + 
		  		"omitted.");
	      System.out.println(" substring Value :" + index);
	      System.out.println("----------------------------------");
	      System.out.println();
	}
	
}
